import React from 'react'
import ReactDom from 'react-dom'
import SearchBar from './components/search_bar'
import YTSearch from 'youtube-api-search'
import VideoList from './components/video_list'
import VideoDetail from './components/video_detail'
import _ from 'lodash'
const API_KEY = "AIzaSyCya8gJFi_WlP8pVQyFulSXlDNQRA2RUGI"


class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      videos: [],
      selectedVideo: null
    }
   this.videoSearch('girl like you')
  }

  videoSearch(term) {
    YTSearch({ key: API_KEY, term: term }, (videos) => { this.setState({ videos, selectedVideo: videos[0] }) })
  }
  render() {
    const videoSearch = _.debounce((term)=> {this.videoSearch(term)},300)
    return (
      <div>
        <SearchBar onSearchTermChange = {videoSearch}/>
        <VideoDetail video={this.state.selectedVideo} />
        <VideoList videos={this.state.videos}
          onVideoSelect={selectedVideo => this.setState({ selectedVideo })} />
      </div>
    );
  }
}

ReactDom.render(<App />, document.querySelector(".container"))
